#!/bin/sh
set -e

if [ "${1#-}" != "$1" ]; then
    set -- traefik "$@"
fi

# Global configuration
set -a -- "$@" "--global.checkNewVersion=${CHECK_NEW_VERSION-false}"
set -a -- "$@" "--global.sendAnonymousUsage=${SEND_ANONYMOUS_USAGE-false}"
set -a -- "$@" "--log.level=${LOG_LEVEL:-ERROR}"
set -a -- "$@" "--api.insecure=${API_INSECURE-false}"
set -a -- "$@" "--api.dashboard=${API_DASHBOAARD-true}"
set -a -- "$@" "--ping=${PING-false}"

# Add providers
if [ "${DOCKER_PROVIDER}" == "true" ]; then
    set -a -- "$@" "--providers.docker=true"
    set -a -- "$@" "--providers.docker.exposedbydefault=${DOCKER_EXXPOSED-true}"
    set -a -- "$@" "--providers.docker.network=${DOCKER_NETWORK-traefik}"
    set -a -- "$@" "--providers.docker.watch=${DOCKER_WATCH-true}"
fi

if [ "${FILE_PROVIDER}" == "true" ]; then
    set -a -- "$@" "--providers.file.directory=${FILE_DIRECTORY-/etc/traefik/dynamic}"
    set -a -- "$@" "--providers.file.watch=${FILE_WATCH-true}"
fi

if [ "${DOCKER_PROVIDER}" == "true" ]; then
    set -a -- "$@" "--providers.rancher=true"
    set -a -- "$@" "--providers.rancher.exposedByDefault=${RANCHER_EXXPOSED-true}"
    set -a -- "$@" "--providers.rancher.watch=${RANCHER_WATCH-true}"
fi

# Add HTTP entrypoint
set -a -- "$@" "--entryPoints.http.address=:${HTTP_PORT:-80}"

# Add HTTPS entrypoint
set -a -- "$@" "--entryPoints.https.address=:${HTTPS_PORT:-443}"
set -a -- "$@" "--entryPoints.https.http.tls=true"

# Headers HTTP
if [ "${FORWARD_HEADERS_INSECURE-false}" == "true" ]; then
    set -a -- "$@" "--entrypoints.http.forwardedheaders.insecure"
    set -a -- "$@" "--entrypoints.https.forwardedheaders.insecure"
fi
if [ "${FORWARD_HEADERS_TRUSTED_IPS}" != "" ]; then
    set -a -- "$@" "--entrypoints.http.forwardedheaders.trustedips=${FORWARD_HEADERS_TRUSTED_IPS}"
    set -a -- "$@" "--entrypoints.https.forwardedheaders.trustedips=${FORWARD_HEADERS_TRUSTED_IPS}"
fi

# Force HTTPS
if [ "${HTTPS_FORCE-false}" == "true" ]; then
    set -a -- "$@" "--entrypoints.http.http.redirections.entryPoint.to=https"
    set -a -- "$@" "--entrypoints.http.http.redirections.entryPoint.scheme=https"
fi

# Acme SSL
if [ "${ACME_ENABLED:-false}" == "true" ]; then
    set -a -- "$@" "--certificatesresolvers.lets-encrypt.acme.caserver=${ACME_CASERVER}"
    set -a -- "$@" "--certificatesresolvers.lets-encrypt.acme.email=${ACME_EMAIL}"
    set -a -- "$@" "--certificatesresolvers.lets-encrypt.acme.storage=/etc/traefik/acme/acme.json"

    set -a -- "$@" "--certificatesresolvers.lets-encrypt.acme.httpchallenge=true"
    set -a -- "$@" "--certificatesresolvers.lets-encrypt.acme.httpchallenge.entrypoint=http"
    if [ ! -z "${ACME_DNS_CHALLENGE}" ]; then
        set -a -- "$@" "--certificatesresolvers.lets-encrypt.acme.dnschallenge=true"
        set -a -- "$@" "--certificatesresolvers.lets-encrypt.acme.dnschallenge.provider=ovh"
    fi
fi

exec "$@"

