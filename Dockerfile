FROM traefik:2.3.4

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
